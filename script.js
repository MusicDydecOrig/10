"use strict";


const personalMovieDB = {
    count : 0,
    movies: {},
    actors: '',
    genres: [],
    privat: false,
    start: function() {
      personalMovieDB.count = +prompt("Сколько фильмов вы уже посмотрели?", "");
      while (numberOfFilms == '' || numberOfFilms == null || isNaN(numberOfFilms)) {
        personalMovieDB.count = +prompt("Сколько фильмов вы уже посмотрели?", "");
      }
    },
    rememberMyFilms: function(){
      for (let i = 0; i < 2; i++) {
        const vop1 = prompt("Один из последних просмотренных фильмов?", ""),
              vop2 = prompt("На сколько оцените его?", "");
    
          if (vop1 != null && vop2 != null && vop1 != '' && vop2 != '' && vop1.length < 50) {
              personalMovieDB.movies[vop1] = vop2;
          } else {
              alert('Ответы так себе');
              i--;
          }
      }
    },
    detectPersonalLevel: function(){
      if (personalMovieDB.count < 10) {
        alert('Просмотрено довольно мало фильмов');
      } else if (personalMovieDB.count >= 10 && personalMovieDB.count < 30) {
        alert('Вы классический зритель');
      } else if (personalMovieDB.count == 30) {
        alert('Вы киноман.');
      } else {
        alert('Произошла ошибка');
      }
    },
    showMyDB: function(hidden){
      if (!hidden) {
        console.log(personalMovieDB);
      }
    },
    writeYourGenres() {
      for (let i = 0; i<=2; i++) {
        let genre = prompt(`Ваш любимый жанр под номером ${i+1}`);
        if (genre == '' || genre == null){
          i--;
        }
        else{
          personalMovieDB.genres[i] = genre;
        }
        personalMovieDB.genres.forEach( (item, i)  => {
          alert(`Любимый жанр #${i+1} - это ${item}`)
        });
      }
    },
    toggleVisibleMyDB: function() {
      if (personalMovieDB.privat) {
        personalMovieDB.privat = false;
      }
      else {
        personalMovieDB.privat = true;
      }
    }
};
